//import {createAll, cleanConsole} from './data.js';

//Modificada importación del modulo data.
var data = require('./data.js');
const companies = data.createAll();

 var companiesModification = function(pCompanies){
    let answer = [];
    for(i in pCompanies)
    {
        //Current companie
        let tempCompanie = pCompanies[i];
        //companie to add to the answer with its valuue modified.
        let companieToAdd = {}; 
        //First modification of the name of the companie so it have the first letter as a capital.
        let nameToAdd = (tempCompanie.name).charAt(0).toUpperCase() + (tempCompanie.name).slice(1);
        let userListToAdd = [];
        let tempUsers = tempCompanie.users;
        tempUsers.forEach(cuser => {
            //User to add with modifications
            let tempUser = {};
            if (typeof (cuser.firstName) === 'undefined')
            {
                tempUser.firstName = '';
            }
            else{
                tempUser.firstName = (cuser.firstName).charAt(0).toUpperCase() + (cuser.firstName).slice(1); 
            }
            if (typeof (cuser.lastName) === 'undefined') {
                tempUser.lastName = '';
            }
            else {
                tempUser.lastName = (cuser.lastName).charAt(0).toUpperCase() + (cuser.lastName).slice(1); 
            }
            if (typeof (cuser.age) === 'undefined') {
                tempUser.age = '';
            }
            else {
                tempUser.age = cuser.age;
            }
            if (typeof (cuser.car) === 'undefined') {
                tempUser.car = '';
            }
            else {
                tempUser.car = cuser.car;
            }
            if (typeof (cuser.id) === 'undefined') {
                tempUser.id = '';
            }
            else {
                tempUser.id = cuser.id;
            }
            //Add  by the name alphabetically
            //iF NO ELEMENT
            let added = false;
            if(userListToAdd.length == 0){
                userListToAdd.push(tempUser);
                added = true;
            }
            else{
            for(j in userListToAdd){
                if(tempUser.firstName<userListToAdd[j].firstName && tempUser.lastName<userListToAdd[j].lastName){
                   userListToAdd.splice(j,0,tempUser);
                   added = true;
                   break;
                }
                if(!added){
                    userListToAdd.push(tempUser);
                    break;
                }
            }}
        });
        //Add companie to answer acording to its user length
        companieToAdd = {
            name: nameToAdd,
            users: userListToAdd,
            isOpen: tempCompanie.isOpen,
            usersLength: tempCompanie.usersLength,
            id: tempCompanie.id
        }
        if (answer.length == 0) {
            answer.push(companieToAdd);
        }
        else {
            for (j in answer) {
                if (companieToAdd.usersLength < answer[j].usersLength) {
                    answer.splice(j, 0, companieToAdd);
                    break;
                }
                else if (answer.length == j) {
                    answer.push(companieToAdd);
                }
            }
        }
    }
    return answer;
}

data.cleanConsole(1, companies);
console.log('---- EXAMPLE 1 --- ', companiesModification(companies));
module.exports = {
    companiesModification: companiesModification
}

// -----------------------------------------------------------------------------
// INSTRUCCIONES EN ESPAÑOL

// Crear una función tomando la variable "companies" como parámetro y reemplazando
// todos los valores "undefined" en "usuarios" por un string vacío.
// El nombre de cada "company" debe tener una letra mayúscula al principio, así como
// el apellido y el nombre de cada "user".
// Las "companies" deben ordenarse por su total de "user" (orden decreciente)
// y los "users" de cada "company" deben aparecer en orden alfabético.

// -----------------------------------------------------------------------------
// INSTRUCTIONS IN ENGLISH

// Create a function taking the variable "companies" as a parameter and replacing
// all values "undefined" in "users" by an empty string.
// The name of each "company" must have a capital letter at the beginning as well as
// the last name and first name of each "user".
// The "companies" must be sorted by their number of "user" (decreasing order)
// and the "users" of each "company" must be listed in alphabetical order.

// -----------------------------------------------------------------------------
// INSTRUCTIONS EN FRANÇAIS

// Créer une fonction prenant en paramètre la variable "companies" et remplaçant
// toutes les valeurs "undefined" dans les "users" par un string vide.
// Le nom de chaque "company" doit avoir une majuscule au début ainsi que
// le nom et le prénom de chaque "user".
// Les "companies" doivent être triées par leur nombre de "user" (ordre décroissant)
// et les "users" de chaque "company" doivent être classés par ordre alphabétique.
